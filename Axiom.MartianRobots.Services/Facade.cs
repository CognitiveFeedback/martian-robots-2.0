﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using Axiom.MartianRobots.Domain;
using Axiom.MartianRobots.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Axiom.MartianRobots.Services
{
	public static class MartianRobotsService
	{
		/// <summary>
		/// Create Robot Simulator from a valid text program.
		/// </summary>
		/// <param name="input">The text input to serve as the simulator program.</param>
		/// <returns>An instance of the RobotSimulator type.</returns>
		public static IRobotSimulator CreateRobotSimulator(string input)
		{
			var simulator = new RobotSimulator(input);
			return simulator;
		}

		/// <summary>
		/// Save to file
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="simulator"></param>
		public static void Save(string filename, IRobotSimulator simulator)
		{
			using (var stream = new FileStream(filename, FileMode.Create, FileAccess.Write))
			{
				BinaryFormatter bf = new BinaryFormatter();
				bf.Serialize(stream, simulator);
			}
		}

		/// <summary>
		/// Load from file
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static IRobotSimulator Load(string filename)
		{
			RobotSimulator result = null;
			using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
			{
				BinaryFormatter bf = new BinaryFormatter();
				result = bf.Deserialize(stream) as RobotSimulator;
			}
			return result;
		}

		/// <summary>
		/// Persist
		/// </summary>
		/// <param name="vector">vector</param>
		public static void Persist(PositionVector vector)
		{
			using (var db = new MartianContext())
			{
				db.Vectors.Add(vector);
				db.SaveChanges();
			}
		}

		/// <summary>
		/// Add
		/// </summary>
		/// <param name="simulator">simulator</param>
		public static void Add(IRobotSimulator simulator)
		{
			if (!(simulator is RobotSimulator))
			{
				throw new InvalidCastException(string.Format("Cannot convert {0} to RobotSimulator", simulator.GetType()));
			}
			Repository.Context.Simulations.Add(simulator as RobotSimulator);
			Repository.Context.SaveChanges();
		}

		/// <summary>
		/// Find
		/// </summary>
		/// <param name="simulator">simulator</param>
		/// <returns></returns>
		public static IRobotSimulator Find(IRobotSimulator simulator)
		{
			if (!(simulator is RobotSimulator))
			{
				throw new InvalidCastException(string.Format("Cannot convert {0} to RobotSimulator", simulator.GetType()));
			}

			var sim = (simulator as RobotSimulator);
			var target = Repository.Context.Simulations.Include("Program").First(a => a.Id == sim.Id);

			return target;
		}
	}
}
