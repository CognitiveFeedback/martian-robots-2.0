﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using System;

namespace Axiom.MartianRobots
{
	public class UnknownOrientationException : Exception
	{
		/// <summary>
		/// Unknown Orientation Exception
		/// </summary>
		/// <param name="message">message</param>
		public UnknownOrientationException(string message)
			: base(message)
		{
		}

	}

	public class InvalidMartianRobotProgramException : Exception
	{
		/// <summary>
		/// Invalid Program Exception
		/// </summary>
		/// <param name="message">message</param>
		public InvalidMartianRobotProgramException(string message)
			: base(message)
		{
		}

	}

	public class GridConfigurationException : Exception
	{
		/// <summary>
		/// Grid Configuration Exception
		/// </summary>
		/// <param name="message">message</param>
		public GridConfigurationException(string message)
			: base(message)
		{
		}
	}

	public class RobotConfigurationException : Exception
	{
		/// <summary>
		/// Grid Configuration Exception
		/// </summary>
		/// <param name="message">message</param>
		public RobotConfigurationException(string message)
			: base(message)
		{
		}
	}
}