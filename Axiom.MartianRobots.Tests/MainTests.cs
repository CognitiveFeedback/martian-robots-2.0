﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using Axiom.MartianRobots;
using Axiom.MartianRobots.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace Axiom.MatrianRobots.Tests
{
	[TestClass]
	public class MainTests
	{
		/// <summary>
		/// Can Create And Run Simulator Program
		/// </summary>
		[TestMethod]
		public void CanCreateAndRunSimulatorProgram()
		{
			var sb = new StringBuilder();
			sb.AppendLine("5 3 ");				// Initialize Grid with Extents

			sb.AppendLine("1 1 E");				// Initialize First Robot with Position and Orientation
			sb.AppendLine("RFRFRFRF ");			// Create Instruction for First Robot

			sb.AppendLine("3 2 N");				// Initialize Second Robot with Position and Orientation
			sb.AppendLine("FRRFLLFFRRFLL");		// Create Instruction for Second Robot

			sb.AppendLine("03 W ");				// Initialize Third Robot with Position and Orientation
			sb.AppendLine(" LLFFFLFLFL");		// Create Instruction for Third Robot

			var input = sb.ToString();

			var simulator = MartianRobotsService.CreateRobotSimulator(input);
			var output = simulator.Run();

			var lines = output.Replace(Environment.NewLine, "\n").Split(new char[] { '\n' });

			Assert.IsTrue(lines[0].Equals("1 1 E"));
			Assert.IsTrue(lines[1].Equals("3 3 N LOST"));
			Assert.IsTrue(lines[2].Equals("2 3 S"));
		}

		/// <summary>
		/// Can Create And Run Simulator Program
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(InvalidMartianRobotProgramException))]
		public void CanCatchSyntaxErrors()
		{
			var sb = new StringBuilder();
			sb.AppendLine("5 3 ");				// Initialize Grid with Extents

			sb.AppendLine(" 03B ");				// Initialize Third Robot with Position and Orientation
			sb.AppendLine(" LLFZFFLFLFL");		// Create Instruction for Third Robot

			var input = sb.ToString();

			var simulator = MartianRobotsService.CreateRobotSimulator(input);

			// expected exception thrown on previous line
			Assert.Fail();
		}
	}
}
