﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using Axiom.MartianRobots.Interfaces;
using Axiom.MartianRobots.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;

namespace Axiom.MatrianRobots.Tests
{
	[TestClass]
	public class PersistenceTests
	{
		/// <summary>
		/// Can Create And Run Simulator Program using BinaryFormatter and the file system.
		/// </summary>
		[TestMethod]
		public void CanSaveAndLoadSimulator_BinaryFormatter()
		{
			var filename = "my-test-file.dat";
			object result = null;

			try
			{
				var simulator = MartianRobotsService.CreateRobotSimulator("5 3\r\n1 1 E\r\nRFRFRFRF");

				MartianRobotsService.Save(filename, simulator);

				result = MartianRobotsService.Load(filename);
			}
			catch (Exception ex)
			{
				Assert.Fail(ex.Message);
			}
			finally
			{
				File.Delete(filename);
			}

			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result, typeof(IRobotSimulator));
		}
	}
}
