﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Axiom.MartianRobots.WPF.ViewModels
{
	[Serializable]
	public class ViewModelBase : INotifyPropertyChanged
	{
		[field:NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;

		protected void NotifyPropertyChanged(string property)
		{
			if (PropertyChanged == null)
			{
				return;
			}
			PropertyChanged(this, new PropertyChangedEventArgs(property));
		}

		[NonSerialized]
		private string _status;

		public string Status
		{
			get
			{
				return _status;
			}
			set
			{
				if (_status == value)
				{
					return;
				}
				_status = value;
				NotifyPropertyChanged("Status");
			}
		}

		[NonSerialized]
		private bool _isBusy;

		public bool IsBusy
		{
			get
			{
				return _isBusy;
			}
			set
			{
				if (_isBusy == value)
				{
					return;
				}
				_isBusy = value;
				NotifyPropertyChanged("IsBusy");
			}
		}

		private static Dispatcher _dispatcher;

		public Dispatcher Dispatcher
		{
			get { return ViewModelBase._dispatcher; }
			set { ViewModelBase._dispatcher = value; }
		}

	}
}
