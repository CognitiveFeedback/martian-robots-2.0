﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axiom.MartianRobots.WPF.ViewModels
{
	public class ViewModelLocator
	{
		private static ViewModel _mainViewModel;

		public static ViewModel GetMainViewModel()
		{
			if (_mainViewModel == null)
			{
				_mainViewModel = new ViewModel();
			}
			return _mainViewModel;
		}
	}
}
