﻿using Axiom.MartianRobots.Domain;
using Axiom.MartianRobots.Services;
using Axiom.MartianRobots.WPF.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

namespace Axiom.MartianRobots.WPF.ViewModels
{
	public class ViewModel : ViewModelBase
	{
		public ViewModel()
		{
			var sb = new StringBuilder();
			sb.AppendLine("5 3 ");
			sb.AppendLine("1 1 E ");
			sb.AppendLine("RFRFRFRF ");
			sb.AppendLine("3 2 N ");
			sb.AppendLine("FRRFLLFFRRFLL");
			sb.AppendLine("03 W ");
			sb.AppendLine("LLFFFLFLFL");
			Input = sb.ToString();
		}

		private string _input;

		public string Input
		{
			get
			{
				return _input;
			}
			set
			{
				if (_input == value)
				{
					return;
				}
				_input = value;
				NotifyPropertyChanged("Input");
			}
		}
		private string _output;

		public string Output
		{
			get
			{
				return _output;
			}
			set
			{
				if (_output == value)
				{
					return;
				}
				_output = value;
				NotifyPropertyChanged("Output");
			}
		}

		private string _result;

		public string Result
		{
			get
			{
				return _result;
			}
			set
			{
				if (_result == value)
				{
					return;
				}
				_result = value;
				NotifyPropertyChanged("Result");
			}
		}

		public async void Execute()
		{
			IsBusy = true;
			Status = "Working";

			await Task.Run(() =>
			{
				try
				{
					Output = string.Empty;
					Result = string.Empty;

					var simulator = MartianRobotsService.CreateRobotSimulator(Input);
					Result = simulator.Run();
				}
				catch (InvalidMartianRobotProgramException ex)
				{
					var sb = new StringBuilder();
					foreach (DictionaryEntry item in ex.Data)
					{
						sb.AppendLine(string.Format("{0} {1}", item.Key, item.Value));
					}
					Output = sb.ToString();
				}
				catch (Exception ex)
				{
					Output = ex.ToString();
				}
			});
			IsBusy = false;
		}
	}
}
