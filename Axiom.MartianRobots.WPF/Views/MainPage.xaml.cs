﻿using Axiom.MartianRobots.WPF.Models;
using Axiom.MartianRobots.WPF.ViewModels;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Navigation;

namespace Axiom.MartianRobots.WPF.Views
{
	public partial class MainPage : Page
	{
		public ViewModel Model { get { return DataContext as ViewModel; } }

		public MainPage()
		{
			InitializeComponent();
			Model.Dispatcher = Dispatcher;
		}

		private void OnExecute(object sender, RoutedEventArgs e)
		{
			Model.Execute();
		}
	}
}