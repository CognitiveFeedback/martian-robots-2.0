﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Windows.Media;

namespace Axiom.MartianRobots.WPF
{
	public partial class Styles : ResourceDictionary
	{
		#region Fields
		public const int HT_CAPTION = 0x2;
		private HwndSource hwndSource;

		private Cursor[] reizeCursors = new Cursor[] 
        { 
            Cursors.SizeWE,
            Cursors.SizeWE,
            Cursors.SizeNS,
            Cursors.SizeNWSE,
            Cursors.SizeNESW,
            Cursors.SizeNS,
            Cursors.SizeNESW,
            Cursors.SizeNWSE
        };
		public const int WM_NCLBUTTONDOWN = 0xA1;
		private const int WM_SYSCOMMAND = 0x112;
		#endregion

		private void Window_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.LeftButton != MouseButtonState.Pressed)
				return;

			Window window = Window.GetWindow(sender as DependencyObject);
			if (window.WindowState == WindowState.Maximized)
			{
				window.WindowState = WindowState.Normal;
			}
			window.DragMove();
		}

		private void OnControlToolboxButtonClick(object sender, RoutedEventArgs e)
		{
			Button bt = sender as Button;
			string tag = bt.Tag.ToString();
			Window currentWindow = Window.GetWindow(bt);

			switch (tag)
			{
				case "minimise":
					currentWindow.WindowState = WindowState.Minimized;
					break;
				case "restore":
					if (currentWindow.WindowState == WindowState.Maximized)
						currentWindow.WindowState = WindowState.Normal;
					else
						currentWindow.WindowState = WindowState.Maximized;
					break;
				case "close":
					currentWindow.Close();
					break;
				default:
					break;
			}
		}

		#region Window
		[DllImport("user32.dll")]
		public static extern bool ReleaseCapture();

		[DllImport("user32.dll")]
		public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

		[DllImport("user32.dll")]
		public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

		public enum ResizeDirection
		{
			Left = 1,
			Right = 2,
			Top = 3,
			TopLeft = 4,
			TopRight = 5,
			Bottom = 6,
			BottomLeft = 7,
			BottomRight = 8
		}
		private void OnDisplayResizeCursor(object sender, MouseEventArgs e)
		{
			//this.Cursor = Cursors.SizeNWSE;
		}

		private void OnMove(object sender, MouseEventArgs e)
		{
			//ReleaseCapture();
			//SendMessage(new WindowInteropHelper(this).Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
		}
	
		private void OnResetCursor(object sender, MouseEventArgs e)
		{
			//if (Mouse.LeftButton != MouseButtonState.Pressed)
			//{
			//    this.Cursor = Cursors.Arrow;
			//}
		}
		
		private void OnResize(object sender, MouseButtonEventArgs e)
		{
			ResizeWindow(ResizeDirection.BottomRight);
		}
		
		private void OnSourceInitiailized(object sender, EventArgs e)
		{
			hwndSource = PresentationSource.FromVisual((Visual)sender) as HwndSource;
		}

		private void ResizeWindow(ResizeDirection direction)
		{
			try
			{
				if (hwndSource == null)
					return;

				SendMessage(hwndSource.Handle, WM_SYSCOMMAND, (IntPtr)(61440 + direction), IntPtr.Zero);
			}
			catch
			{
			}
		}
		#endregion

		private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ClickCount != 2)
				return;
			Window window = Window.GetWindow(sender as DependencyObject);
			if (window.WindowState == WindowState.Maximized)
			{
				window.WindowState = WindowState.Normal;
			}
			else
			{
				window.WindowState = WindowState.Maximized;
			}
		}

		private void Canvas_MouseDown_1(object sender, MouseButtonEventArgs e)
		{

		}
	}
}
