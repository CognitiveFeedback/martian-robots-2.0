﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Axiom.MartianRobots.WPF.Converters
{
	public static class DefaultImage
	{
		static DefaultImage()
		{
			BitmapImage bitmap = new BitmapImage();

			bitmap.BeginInit();
			bitmap.UriSource = new Uri("pack://application:,,,/Axiom.MartianRobots.WPF;component/Assets/Images/Thumbnails/pdf-thumb.png");			
			bitmap.EndInit();			
			bitmap.Freeze();

			Image = bitmap;
		}
		public static ImageSource Image { get; set; }		
	}

	[ValueConversion(typeof(object), typeof(ImageSource))]
	public class NullToDefaultImage : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
			{

				return DefaultImage.Image;
			}
			else
			{
				return value;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return DependencyProperty.UnsetValue;
		}
	}

	[ValueConversion(typeof(string), typeof(string))]
	public class EmptryToUnknownCategory : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (string.IsNullOrWhiteSpace((string)value))
			{

				return "Unknown";
			}
			else
			{
				return value;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return value;
		}
	}
}


