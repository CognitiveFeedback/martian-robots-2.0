﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Axiom.MartianRobots.WPF
{
	public static class DrawingExtensions
	{
		private static int _512 = 512;

		public static Image CreateThumb(this Image img)
		{
			Image result = null;

			double aspectRatio = (double)img.Width / (double)img.Height;
			int width;
			int height;

			if (img.Height < _512 && img.Width < _512)
			{
				return img;
			}

			if (img.Width > img.Height)
			{
				width = (int)(aspectRatio * _512);
				height = _512;
			}
			else
			{
				width = _512;
				height = (int)(1d / aspectRatio * _512);
			}

			result = img.GetThumbnailImage(width, height, null, IntPtr.Zero);
			img.Dispose();
			return result;
		}

		private static Image cropImage(Image img, Rectangle cropArea)
		{
			Bitmap bmpImage = new Bitmap(img);
			Bitmap bmpCrop = bmpImage.Clone(cropArea, bmpImage.PixelFormat);
			return (Image)(bmpCrop);
		}
	}
}
