﻿using Axiom.MartianRobots.WPF.ViewModels;
using System;
using System.Windows.Media;

namespace Axiom.MartianRobots.WPF.Models
{
	[Serializable]
	public class Book : ViewModelBase
	{
		public string _name;

		private string _author;

		private string _category;

		private byte[] _checksum;

		private DateTime _dateAdded = DateTime.Now;

		private Guid _id = Guid.NewGuid();

		[NonSerialized]
		private ImageSource _image;

		private string _keywords;

		private string _metadata;

		private string _subject;

		public string Author
		{
			get
			{
				return _author;
			}
			set
			{
				if (_author == value)
				{
					return;
				}
				_author = value;
				NotifyPropertyChanged("Author");
			}
		}

		public string Category
		{
			get
			{
				return _category;
			}
			set
			{
				if (_category == value)
				{
					return;
				}
				if (string.IsNullOrWhiteSpace(value))
				{
					value = "Unknown";
				}
				_category = value;
				NotifyPropertyChanged("Category");
			}
		}

		public byte[] Checksum
		{
			get
			{
				return _checksum;
			}
			set
			{
				_checksum = value;
			}
		}

		public DateTime DateAdded
		{
			get
			{
				return _dateAdded;
			}
			set
			{
				if (_dateAdded == value)
				{
					return;
				}
				_dateAdded = value;
				NotifyPropertyChanged("DateAdded");
			}
		}

		public Guid Id
		{
			get
			{
				if (_id == Guid.Empty)
				{
					_id = Guid.NewGuid();
				}
				return _id;
			}
		}

		public ImageSource Image
		{
			get
			{
				return _image;
			}
			set
			{
				if (object.ReferenceEquals(_image, value))
				{
					return;
				}
				_image = value;
				NotifyPropertyChanged("Image");
			}
		}

		public string Keywords
		{
			get
			{
				return _keywords;
			}
			set
			{
				if (_keywords == value)
				{
					return;
				}
				_keywords = value;
				NotifyPropertyChanged("Keywords");
			}
		}

		public string Metadata
		{
			get
			{
				return _metadata;
			}
			set
			{
				if (_metadata == value)
				{
					return;
				}
				_metadata = value;
				NotifyPropertyChanged("Metadata");
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				if (_name == value)
				{
					return;
				}
				_name = value;
				NotifyPropertyChanged("Name");
			}
		}

		public string Path { get; set; }

		public bool Remove { get; set; }

		public string Subject
		{
			get
			{
				return _subject;
			}
			set
			{
				if (_subject == value)
				{
					return;
				}
				_subject = value;
				NotifyPropertyChanged("Subject");
			}
		}

		public Book()
		{
			Name = string.Empty;
			Category = string.Empty;
			Path = string.Empty;
		}
	}
}
