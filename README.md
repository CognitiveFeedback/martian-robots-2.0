This is a C# solution to the classic Martian Robot simulator challenge.

The core solution consists of three .NET assemblies: two auxiliary modules, and the main core logic.
The first assembly—Axiom.MartianRobots.Interfaces—publicly exposes a set of interfaces that define the consumer access to the main functionality.

A second assembly—Axiom.MartianRobots.Services—implements a WCF service to provide physical access to system functionality. (In the provided demo code a façade class is used to instantiate an instance of the main type RobotSimulator type. 

The third assembly—Axiom.Martianrobots.Domain—represents the core system logic. This assembly defines the Domain Model (i.e. the logical Data Entities and the main business logic or algorithm).

The consumer provides Input as a text parameter defining a valid program to be executed on Robot Simulator. On instantiation, the simulator validates the input as a valid robot simulator program and throws relevant exceptions for invalid cases. Exceptions contain information regarding the nature of the error including syntax errors etc.

Persistence is provided by Entity Framework Code-First, but also with Binary Formatter Serialization. This provides flexibility and efficiency during development and testing. 

Precise sample-data covering explicit test cases is supplied by strict analysis of the problem and definition of the boundary conditions. In addition: large sample-data-sets are generated by a supplementary class library to facilitate load and statistical metrics.
