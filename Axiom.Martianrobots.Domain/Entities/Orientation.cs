﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Axiom.MartianRobots.Domain
{
	/// <summary>
	/// Orientation enumeration for the four directions
	/// </summary>
	public enum Orientation
	{
		North = 0,
		East = 1,
		South = 2,
		West = 3,
	}

	public static class OrientationExtensions
	{
		/// <summary>
		/// To Orientation
		/// </summary>
		/// <param name="value">value</param>
		/// <returns></returns>
		public static Orientation ToOrientation(this Group value)
		{
			Orientation result;

			switch (value.ToString())
			{
				case "N":
					result = Orientation.North;
					break;
				case "W":
					result = Orientation.West;
					break;
				case "S":
					result = Orientation.South;
					break;
				case "E":
					result = Orientation.East;
					break;
				default:
					throw new UnknownOrientationException(value.ToString());
			}

			return result;
		}
	}
}
