﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Axiom.MartianRobots.Domain
{
	[Serializable]
	public class Program : BaseClass
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		public Program()
		{
		}

		/// <summary>
		/// Program
		/// </summary>
		/// <param name="input">The input code to be compiled and executed.</param>
		public Program(string input)
		{
			_input = input;
			_instructions = input.ToProgram();
		}

		private string _input;

		public string Input
		{
			get { return _input; }
			set { _input = value; }
		}

		private Queue<string> _instructions;

		/// <summary>
		/// Gets or Sets the Instruction list to be executed
		/// </summary>
		[NotMapped]
		public Queue<string> Instructions
		{
			get
			{
				return _instructions;
			}
			set
			{
				if (_instructions == null && !string.IsNullOrEmpty(_input))
				{
					_instructions = _input.ToProgram();
				}
				_instructions = value;
			}
		}

		/// <summary>
		/// Gets or Sets the list of validation errors.
		/// </summary>
		[NotMapped]
		public StringDictionary ValidationErrors { get; set; }

		/// <summary>
		/// Get a value indicating whether any errors exist.
		/// </summary>
		[NotMapped]
		public bool HasErrors
		{
			get
			{
				var hasErrors = ValidationErrors != null && ValidationErrors.Count > 0;
				return hasErrors;
			}
		}

		/// <summary>
		/// Validate the current Instruction list.
		/// </summary>
		/// <returns>True if there exists any errors otherwise False.</returns>
		public bool Validate()
		{
			var lines = Instructions.ToArray();

			// Valid Pattern e.g. ' 1 23 E '  and  '123E'  (this latter case was forced by the sample input supplied by the question description
			var vailidRobotInitializer = new Regex(@"^[ ]*(?<x>\d{1,2})[ ]*(?<y>\d{1,2})[ ]*(?<orientation>[NSEW])[ ]*$", RegexOptions.Compiled);

			// Valid Pattern e.g. 'LLLRFRF'
			var vailidRobotInstruction = new Regex(@"^[LRF]+$", RegexOptions.Compiled);

			ValidationErrors = new StringDictionary();

			if (lines.Length % 2 != 1)
			{
				ValidationErrors.Add("Program Length", string.Format("Invalid program line count: {0}", lines.Length));
			}
			for (int i = 2; i < lines.Length; i += 2)
			{
				if (lines[i].Length > 100)
				{
					ValidationErrors.Add(string.Format("Line {0}: Instruction Length.", i + 1, lines[i]), lines[i]);
				}
				if (!vailidRobotInstruction.Match(lines[i]).Success)
				{
					ValidationErrors.Add(string.Format("Line {0}: Instruction Syntax Error.", i + 1, lines[i]), lines[i]);
				}
			}
			for (int i = 1; i < lines.Length; i += 2)
			{
				if (!vailidRobotInitializer.Match(lines[i]).Success)
				{
					ValidationErrors.Add(string.Format("Line {0}: Robot Initialization Syntax Error.", i + 1, lines[i]), lines[i]);
				}
			}

			return HasErrors;
		}
	}
}
