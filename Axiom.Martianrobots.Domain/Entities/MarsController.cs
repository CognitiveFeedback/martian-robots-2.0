﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Axiom.MartianRobots.Domain
{
	[Serializable]
	public class MarsController : BaseClass
	{
		private IList<PositionVector> _dangerousVectors = new List<PositionVector>();

		/// <summary>
		/// Collection of Lasdt-Known-PostionVectors when robots were LOST
		/// </summary>
		public IList<PositionVector> LostVectors
		{
			get { return _dangerousVectors; }
			private set { _dangerousVectors = value; }
		}

		private int left;  // Represents the left-most (or largest value x-coordinate) of the zero-indexed Martian Surface Grid array.

		private int top;	// Represents the top-most (or largest value y-coordinate) of the zero-indexed Martian Surface Grid array.

		private Robot _currentRobot;

		/// <summary>
		/// Current Robot
		/// </summary>
		public Robot CurrentRobot
		{
			get { return _currentRobot; }
			set { _currentRobot = value; }
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		public MarsController()
		{
		}

		/// <summary>
		/// Mars Surface Grid
		/// </summary>
		/// <param name="left">left</param>
		/// <param name="top">top</param>
		public MarsController(int left, int top)
		{
			this.left = left;	// subtract one to correctly represent the zero-index value
			this.top = top;		// subtract one to correctly represent the zero-index value
		}

		/// <summary>
		/// Act
		/// Execute a single robot instruction
		/// </summary>
		/// <param name="action">action</param>
		public void Act(char action)
		{
			switch (action)
			{
				case 'L':
					CurrentRobot.TurnLeft();
					break;
				case 'R':
					CurrentRobot.TurnRight();
					break;
				case 'F':
					MoveRobotForward();
					break;
				default:
					throw new InvalidOperationException(action.ToString());
			}
		}
		
		/// <summary>
		/// Execute Instruction
		/// Executes the given sequence of instructions for the current robot.
		/// </summary>
		/// <param name="instruction">The sequence of robot instructions to be executed.</param>
		/// <returns></returns>
		public string ExecuteInstruction(string instruction)
		{
			var result = string.Empty;

			var lost = false;
			var lastPosition = new PositionVector(CurrentRobot.Position);
			foreach (var action in instruction)
			{
				Act(action);

				// determine whather robot has falled of edge
				if (CurrentRobot.Position.X > this.left || CurrentRobot.Position.Y > this.top || CurrentRobot.Position.X < 0 || CurrentRobot.Position.Y < 0)
				{
					lost = true;

					LostVectors.Add(lastPosition);

					break;
				}
				lastPosition = new PositionVector(CurrentRobot.Position);
			}

			if (lost)
			{
				result = string.Format("{0} LOST", lastPosition);
			}
			else
			{
				result = lastPosition.ToString();
			}

			return result;
		}

		/// <summary>
		/// Move Robot Forward
		/// Validates a move forward by checking the history of 'LOST' signals from the current robot's position and orientation
		/// If a previous robot was lost under the same conditions, then the instruction is ignored. Otherwise
		/// the robot is instructed to move forward.
		/// </summary>
		private void MoveRobotForward()
		{
			if(LostVectors.Any(a=>a.Equals(CurrentRobot.Position)))
			{
				return;
			}

			CurrentRobot.MoveForward();
		}
	}
}
