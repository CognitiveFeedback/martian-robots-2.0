﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Axiom.MartianRobots.Domain
{
	[Serializable]
	public class PositionVector : BaseClass, IEquatable<PositionVector>
	{
		private Orientation _orientation;

		private int _x;

		private int _y;

		[Column("Orientation", TypeName = "Int")]
		public Orientation Orientation
		{
			get { return _orientation; }
			set { _orientation = value; }
		}

		public int X
		{
			get { return _x; }
			set { _x = value; }
		}

		public int Y
		{
			get { return _y; }
			set { _y = value; }
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		public PositionVector()
		{
		}

		/// <summary>
		/// Grid Vector
		/// </summary>
		/// <param name="v">v</param>
		public PositionVector(PositionVector v)
		{
			_x = v.X;
			_y = v.Y;
			_orientation = v.Orientation;
		}

		/// <summary>
		/// Grid Vector
		/// </summary>
		/// <param name="x">x</param>
		/// <param name="y">y</param>
		/// <param name="orientation">orientation</param>
		public PositionVector(int x, int y, Orientation orientation)
		{
			_x = x;
			_y = y;
			_orientation = orientation;
		}

		/// <summary>
		/// To String
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return string.Format("{0} {1} {2}", X, Y, Orientation.ToString()[0]);
		}

		/// <summary>
		/// Equals
		/// </summary>
		/// <param name="other">other</param>
		/// <returns></returns>
		public bool Equals(PositionVector other)
		{
			return _x == other.X && _y == other.Y && Orientation == other.Orientation;
		}
	}
}
