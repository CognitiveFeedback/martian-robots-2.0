﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Threading;
using System.ComponentModel.DataAnnotations.Schema;

namespace Axiom.MartianRobots.Domain
{
	[Serializable]
	public class BaseClass
	{
		protected DateTime _createDateTime = DateTime.Now;
		protected String _createUser = Thread.CurrentPrincipal.Identity.Name;
		protected Guid _id;

		/// <summary>
		/// Base Class
		/// </summary>
		public BaseClass()
		{
			if (string.IsNullOrEmpty(_createUser))
			{
				_createUser = Environment.UserName;
			}
		}

		[Required]
		public DateTime CreatedDateTime
		{
			get
			{
				return _createDateTime;
			}
			set
			{
				_createDateTime = value;
			}
		}

		[Required]
		public String CreateUser
		{
			get { return _createUser; }
			set { _createUser = value; }
		}

		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public virtual Guid Id
		{
			get
			{
				return _id;
			}
			set
			{
				if (_id != value)
				{
					_id = value;
				}
			}
		}
	}
}
