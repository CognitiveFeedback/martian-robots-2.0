﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using System;
using System.Collections.Generic;

namespace Axiom.MartianRobots.Domain
{
	[Serializable]
	public class Robot : BaseClass
	{
		private PositionVector position;

		/// <summary>
		/// Gets or Sets the current position of a robot.
		/// </summary>
		public PositionVector Position
		{
			get { return position; }
			set { position = value; }
		}

		/// <summary>
		/// Default Constructor
		/// </summary>
		public Robot()
		{
		}

		/// <summary>
		/// Robot Constructor overload
		/// </summary>
		/// <param name="x">x</param>
		/// <param name="y">y</param>
		/// <param name="orientation">orientation</param>
		public Robot(int x, int y, Orientation orientation)
		{
			Position = new PositionVector(x, y, orientation);
		}

		/// <summary>
		/// Turn Left
		/// </summary>
		public void TurnLeft()
		{
			switch (Position.Orientation)
			{
				case Orientation.North:
					position.Orientation = Orientation.West;
					break;
				case Orientation.West:
					position.Orientation = Orientation.South;
					break;
				case Orientation.South:
					position.Orientation = Orientation.East;
					break;
				case Orientation.East:
					position.Orientation = Orientation.North;
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Turn Right
		/// </summary>
		public void TurnRight()
		{
			switch (Position.Orientation)
			{
				case Orientation.North:
					position.Orientation = Orientation.East;
					break;
				case Orientation.West:
					position.Orientation = Orientation.North;
					break;
				case Orientation.South:
					position.Orientation = Orientation.West;
					break;
				case Orientation.East:
					position.Orientation = Orientation.South;
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Move Forward
		/// </summary>
		public void MoveForward()
		{
			switch (Position.Orientation)
			{
				case Orientation.North:
					position.Y += 1;
					break;
				case Orientation.West:
					position.X -= 1;
					break;
				case Orientation.South:
					position.Y -= 1;
					break;
				case Orientation.East:
					position.X += 1;
					break;
				default:
					break;
			}
		}
	}
}
