﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Axiom.MartianRobots.Domain
{
	public static class Extensions
	{
		/// <summary>
		/// To Lines
		/// Transforms multiline text into a List of strings
		/// </summary>
		/// <param name="value">value</param>
		/// <returns></returns>
		public static List<string> ToLines(this string value)
		{
			return value.ToProgram().ToList();
		}

		/// <summary>
		/// To Program
		/// Transforms multiline text into a queued collection of strings
		/// </summary>
		/// <param name="value">value</param>
		/// <returns></returns>
		public static Queue<string> ToProgram(this string value)
		{
			var result = new Queue<string>();

			using (var reader = new StringReader(value))
			{
				while (reader.Peek() != -1)
				{
					var line = reader.ReadLine().Trim();
					if (string.IsNullOrWhiteSpace(line))
					{
						continue;
					}
					result.Enqueue(line);
				}
			}

			return result;
		}

	}
}
