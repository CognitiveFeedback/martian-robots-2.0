﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace Axiom.MartianRobots.Domain
{
	public class DropCreatePopulateDatabase : IDatabaseInitializer<MartianContext>
	{
		private MartianContext Context { get; set; }

		public void InitializeDatabase(MartianContext context)
		{
			Context = context;

			Database.SetInitializer(new DropCreateDatabaseIfModelChanges<MartianContext>());

			if (!context.Database.Exists() || context.Database.Exists() && !context.Database.CompatibleWithModel(false))
			{
				context.Database.Delete();
				context.Database.Create();
				Populate();
			}
		}

		private void Populate()
		{
			AddImages();
		}

		private void AddImages()
		{
			
		}
	}
}
