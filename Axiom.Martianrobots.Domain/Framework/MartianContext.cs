﻿using Axiom.MartianRobots.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axiom.MartianRobots.Domain
{
	public class MartianContext : DbContext
	{
		public DbSet<RobotSimulator> Simulations { get; set; }
		public DbSet<MarsController> Controllers { get; set; }
		public DbSet<Robot> Robots { get; set; }
		public DbSet<PositionVector> Vectors { get; set; }
		public DbSet<Program> Programs { get; set; }
	}
}
