﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Axiom.MartianRobots.Domain
{
	public class Repository
	{
		private static MartianContext _context;
		private static Repository _instance;

		static Repository()
		{
			_instance = new Repository();
			_context = new MartianContext();
			Database.SetInitializer(new DropCreatePopulateDatabase()); 			
		}

		public static void DropCreateDatabase()
		{
			try
			{
				if (Database.Exists(typeof(MartianContext).Name))
				{
					Database.Delete(typeof(MartianContext).Name);
				}

				Database.SetInitializer(new DropCreateDatabaseAlways<MartianContext>());
				_context = new MartianContext();
				new DropCreatePopulateDatabase().InitializeDatabase(_context);
			}
			catch
			{
				// ignore
				System.Diagnostics.Debugger.Break();
			}
		}

		public static MartianContext Create()
		{
			_context = new MartianContext();
			return _context;
		}

		private Repository()
		{
		}

		public static MartianContext Context
		{
			get { return _context; }
			set { _context = value; }
		}

		public static Repository Instance { get { return _instance; } }

		public void SaveOrUpdate(dynamic o)
		{
			try
			{
				if (o.Id == 0)
					Context.Set(o.GetType()).Add(o);
				Context.SaveChanges();
			}
			catch (DbEntityValidationException dbEx)
			{
				foreach (var validationErrors in dbEx.EntityValidationErrors)
				{
					foreach (var validationError in validationErrors.ValidationErrors)
					{
						Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
					}
				}
			} 

		}
	}
}
