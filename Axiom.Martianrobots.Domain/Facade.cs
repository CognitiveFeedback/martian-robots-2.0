﻿//===================================================================================
// Axiom Development
//===================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================
// Copyright (c) Axiom Corporation.  All Rights Reserved.
// This code is released under the terms of the MS-LPL license, 
// http://axiommanifold.net
//===================================================================================

using Evolve.MartianRobots.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Evolve.MartianRobots.Domain
{
	public static class Facade
	{
		private static int MAX_EXTENT = 50;

		/// <summary>
		/// Create a Grid from a line of Program text
		/// </summary>
		/// <param name="line">A line of Program text used to create a Martian Surface Grid.</param>
		/// <returns>An instance of the MarsController type.</returns>
		public static MarsController CreateMarsController(string line)
		{
			// match: x, y
			var regex = new Regex(@"^[ ]*(?<x>\d{1,2})[ ]+(?<y>\d{1,2})[ ]*$");
			var match = regex.Match(line);
			if (!match.Success)
			{
				throw new GridConfigurationException(line);
			}

			var x = int.Parse(match.Groups["x"].ToString());
			var y = int.Parse(match.Groups["y"].ToString());

			if (x > MAX_EXTENT || y > MAX_EXTENT || x < 0 || y < 0)
			{
				throw new GridConfigurationException(line);
			}

			var grid = new MarsController(x, y);
			return grid;
		}

		/// <summary>
		/// Create a Robot from a line of Program text.
		/// </summary>
		/// <param name="line">A line of Program text used to create and initialize a Robot.</param>
		/// <returns>An instance of the Robot type.</returns>
		public static Robot CreateRobot(string line)
		{
			// match: x, y, orientation
			var regex = new Regex(@"^[ ]*(?<x>\d{1,2})[ ]*(?<y>\d{1,2})[ ]*(?<orientation>[NSEW])[ ]*$");
			var match = regex.Match(line);
			if (!match.Success)
			{
				throw new RobotConfigurationException(line);
			}

			var x = int.Parse(match.Groups["x"].ToString());
			var y = int.Parse(match.Groups["y"].ToString());
			var orientation = match.Groups["orientation"].ToOrientation();

			if (x > MAX_EXTENT || y > MAX_EXTENT || x < 0 || y < 0)
			{
				throw new RobotConfigurationException(line);
			}

			var robot = new Robot(x, y, orientation);
			return robot;
		}

		/// <summary>
		/// Create Robot Simulator from a valid text program.
		/// </summary>
		/// <param name="input">The text input to serve as the simulator program.</param>
		/// <returns>An instance of the RobotSimulator type.</returns>
		public static IRobotSimulatorService CreateRobotSimulator(string input)
		{
			var simulator = new RobotSimulator(input);
			return simulator;
		}

		/// <summary>
		/// Save to file
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="simulator"></param>
		public static void Save(string filename, RobotSimulator simulator)
		{
			using (var stream = new FileStream(filename, FileMode.Create, FileAccess.Write))
			{
				BinaryFormatter bf = new BinaryFormatter();
				bf.Serialize(stream, simulator);
			}
		}

		/// <summary>
		/// Load from file
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static RobotSimulator Load(string filename)
		{
			RobotSimulator result = null;
			using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
			{
				BinaryFormatter bf = new BinaryFormatter();
				result = bf.Deserialize(stream) as RobotSimulator;
			}
			return result;
		}

		/// <summary>
		/// Persist
		/// </summary>
		/// <param name="vector">vector</param>
		public static void Persist(PositionVector vector)
		{
			using (var db = new MartianContext())
			{
				db.Vectors.Add(vector);
				db.SaveChanges();
			}
		}

		/// <summary>
		/// Add
		/// </summary>
		/// <param name="simulator">simulator</param>
		public static void Add(IRobotSimulatorService simulator)
		{
			if (!(simulator is RobotSimulator))
			{
				throw new InvalidCastException(string.Format("Cannot convert {0} to RobotSimulator", simulator.GetType()));
			}
			Repository.Context.Simulations.Add(simulator as RobotSimulator);
			Repository.Context.SaveChanges();
		}

		/// <summary>
		/// Find
		/// </summary>
		/// <param name="simulator">simulator</param>
		/// <returns></returns>
		public static IRobotSimulatorService Find(IRobotSimulatorService simulator)
		{
			if (!(simulator is RobotSimulator))
			{
				throw new InvalidCastException(string.Format("Cannot convert {0} to RobotSimulator", simulator.GetType()));
			}

			var sim = (simulator as RobotSimulator);
			var target = Repository.Context.Simulations.Include("Program").First(a => a.Id == sim.Id);

			return target;
		}
	}
}
